ASM = nasm
LD = ld

all: client.asm
	$(ASM) -f elf64 client.asm -o client.o
	$(LD) client.o -dynamic-linker /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2 -o client

clean:
	@rm -f *.o client
