;extern gethostbyname

section .data
	
	connecting		db	"Connecting...",0xA
	connecting_len		equ	$ - connecting

	connected		db	"Connected.",0xA
	connected_len		equ	$ - connected

	connection_failed	db	"Connection failed!",0xA
	connection_failed_len	equ	$ - connection_failed

	sending			db	"Sending data.",0xA
	sending_len		equ	$ - sending

	closing			db	"Closing connection...",0xA
	closing_len		equ	$ - closing

	closed			db	"Connection closed.", 0xA
	closed_len		equ	$ - closed

	connection_error	db	"Connection error.", 0xA
	connection_error_len	equ	$ - connection_error

	socket_create_error	db	"Could not create socket.", 0xA
	socket_create_error_len	equ	$ - socket_create_error

	no_parameters		db	"Usage: ./client <port>", 0xA
	no_parameters_len	equ	$ - no_parameters

	input			db	"> "
	input_len		equ	$ - input
	
	output			db	"Recived: "
	output_len		equ	$ - output

	sockaddr:
				dw	2		; AF_INET
				dw	0		; PORT
				dd	0x0100007F	; IP 127.0.0.1
				dq	0		; ZEROS
	sockaddr_len		equ	$ - sockaddr
	hostname:		db	"localhost", 0
section .bss
	PORT			resw	1
	msg			resb	256
	msg_recv		resb	256

section .text
	global _start

_start:
	;lea	rdi, [hostname]
	;int3
	;call	gethostbyname
	;int3
	mov	rax, [rsp]
	cmp	rax, 2 
	jne	_error_param_exit
	
	call _arg
_main:
	call 	_socket
	cmp	rax, 0
	jl	_error_socket_exit	

	call 	_connect

	cmp 	rax, 0
	jl	_error_conn_exit	

	call	_print_input
	call 	_read
	call 	_send
	call	_print_output
	call 	_recv
	call	_close
	call 	_exit

_print_input:
	push	rdi
	mov	rax, 1
	mov	rdi, 1
	mov	rsi, input
	mov	rdx, input_len
	syscall
	pop	rdi
	ret

_print_output:
	push	rdi
	mov	rax, 1
	mov	rdi, 1
	mov	rsi, output
	mov	rdx, output_len
	syscall
	pop	rdi
	ret

_read:
	push 	rdi
	mov	rax, 0
	mov	rdi, 0
	mov	rsi, msg
	mov	rdx, 256
	syscall
	pop 	rdi
	ret

_arg:
	mov	rax, [rsp+24]
	mov	rbx, [rax]
	mov	[PORT], ebx
	mov	esi, PORT
	mov	rax, 0
	mov	rbx, 0
	jmp	_atoi

_atoi:
	mov	bl, [esi]
	
	cmp	bl, 10
	cmp	bl, 0
	je _atoi_end
	
	imul	eax, 10
	sub	bl, 0x30
	add	eax, ebx	
	inc	esi
	jmp	_atoi


_atoi_end:
	mov	rbx, sockaddr
	xchg	al, ah
	mov	[sockaddr+2], ax
	jmp _main

_error_param_exit:
	mov	rax, 1
	mov	rdi, 1
	mov	rsi, no_parameters
	mov	rdx, no_parameters_len
	syscall
	call 	_error_exit

_error_socket_exit:
	mov	rax, 1
	mov	rdi, 1
	mov	rsi, socket_create_error
	mov	rdx, socket_create_error_len
	syscall
	call 	_error_exit

_error_conn_exit:
	mov	rax, 1
	mov	rdi, 1
	mov	rsi, connection_error
	mov	rdx, connection_error_len
	syscall
	call 	_error_exit

_error_exit:
	mov	rax, 60
	mov 	rdi, 1
	syscall

_exit:
	mov	rax, 60
	mov	rdi, 0
	syscall

_recv:
	mov	rax, 0
	mov	rsi, msg_recv
	mov	rdx, 256
	syscall

	push rdi
	push rax
	mov	rax, 1
	mov	rdi, 1
	mov	rsi, msg_recv
	pop	rdx
	syscall
	
	pop rdi
	ret

_send:
	mov	rax, 1
	mov	rsi, msg
	mov	rdx, 256
	syscall
	ret

_close:
	mov	rax, 3
	syscall
	ret

_connect:
	mov	rax, 42
	mov	rsi, sockaddr
	mov	rdx, sockaddr_len
	syscall
	ret

_socket:
	mov	rax, 41		; sys_socket
	mov	rsi, 1		; SOCK_STREAM
	mov	rdi, 2		; AF_INET
	mov	rdx, 0		; IPPROTO_IP
	syscall

	mov	rdi, rax
	ret
